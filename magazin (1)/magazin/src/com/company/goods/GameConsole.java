package com.company.goods;

public class GameConsole {
    private String name;
    private boolean hasGuarantee;
    private String ram;
    private String department;
    private String price;
    private String company;

    public GameConsole(String name, boolean hasGuarantee, String ram, String department, String price, String company) {
        this.name = name;
        this.hasGuarantee = hasGuarantee;
        this.ram = ram;
        this.department = department;
        this.price = price;
        this.company = company;
    }

    public void on(){

    }
    public void loadGame(){

    }

    public boolean isHasGuarantee() {
        return hasGuarantee;
    }
}

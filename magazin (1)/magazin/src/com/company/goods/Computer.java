package com.company.goods;

public class Computer {
    private String name;
    private boolean hasGuarantee;
    private String ram;
    private String department;
    private String price;
    private String company;

    public Computer(String name, boolean hasGuarantee, String ram, String department, String price, String company) {
        this.name = name;
        this.hasGuarantee = hasGuarantee;
        this.ram = ram;
        this.department = department;
        this.price = price;
        this.company = company;
    }

    public void on(){

    }
    public void off(){

    }
    public void loadOS(){

    }

    public boolean isHasGuarantee() {
        return hasGuarantee;
    }

}

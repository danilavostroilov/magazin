package com.company.service;

public class Administrator {
    private String name;
    private String salesRoom;
    private boolean free;

    public Administrator(String name, String salesRoom, boolean free) {
        this.name = name;
        this.salesRoom = salesRoom;
        this.free = free;
    }

    public void getFreeConsultant() {

    }

    public boolean isFree() {
        return free;
    }
}

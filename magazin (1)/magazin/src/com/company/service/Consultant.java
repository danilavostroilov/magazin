package com.company.service;

public class Consultant {
    private String name;
    private boolean free;
    private String department;

    public Consultant(String name, boolean free, String department) {
        this.name = name;
        this.free = free;
        this.department = department;
    }

    public void consult(){

    }
    public void send(){

    }

    public boolean isFree() {
        return free;
    }
}

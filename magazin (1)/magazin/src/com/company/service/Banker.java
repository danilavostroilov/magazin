package com.company.service;

public class Banker {
    private String name;
    private boolean free;
    private String department;

    public Banker(String name, boolean free, String department) {
        this.name = name;
        this.free = free;
        this.department = department;
    }

    public void sendRequest(){

    }

    public boolean isFree() {
        return free;
    }
}
